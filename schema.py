from uuid import UUID
from typing import List, Optional
from datetime import datetime
from pydantic import BaseModel
from enum import IntEnum


class Account(BaseModel):
    currency : str
    balance : float


class User(BaseModel):
    accounts : List[Account]


class Operation(BaseModel):
    operation : str
    currency : str
    src_user_id : UUID
    dst_user_id : Optional[UUID] = None
    created_at : datetime
