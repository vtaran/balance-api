import uvicorn
from fastapi import FastAPI
from schema import User as SchemaUser
from schema import Operation as SchemaOperation
from schema import Account as SchemaAccount
from models import User, Operation, Account, CurrencyTypes
from fastapi_sqlalchemy import DBSessionMiddleware, db
from config import DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME


app = FastAPI()
app.add_middleware(DBSessionMiddleware, db_url=f'postgresql+psycopg2://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}')


@app.get('/')
def index():
    return {'status': 'ok'}


@app.get('/users/')
def get_users():
    users = db.session.query(User).all()

    return users


@app.post('/add_user/', response_model=SchemaUser)
def add_user(user: SchemaUser):
    print(user)

    db_user = User()
    db.session.add(User)
    db.session.commit()

    return user


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)
