import enum
from sqlalchemy import Column, DateTime, ForeignKey, Integer, Float, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


Base = declarative_base()


class CurrencyTypes(enum.Enum):
    EUR = 1
    USD = 2
    BTC = 3
    ETH = 4


class OperationType(enum.Enum):
    credit = 1
    debit = 2
    transfer = 3


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, index=True)
    accounts = relationship('Account')

    class Config:
        orm_mode = True


class Account(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True, index=True)
    currency = Column(Enum(CurrencyTypes))
    balance = Column(Float)
    user_id = Column(Integer, ForeignKey('user.id'))

    class Config:
        orm_mode = True


class Operation(Base):
    __tablename__ = 'opertaion'
    id = Column(Integer, primary_key=True, index=True)
    operation = Column(Enum(OperationType))
    currency = Column(Enum(CurrencyTypes))
    src_user_id = (Integer, ForeignKey('user.id'))
    dst_user_id = (Integer, ForeignKey('user.id'))

    class Config:
        orm_mode = True
